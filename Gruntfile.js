'use strict';
module.exports = function(grunt) {
  // Load all tasks
  require('load-grunt-tasks')(grunt);
  // Show elapsed time
  require('time-grunt')(grunt);

  var jsFileList = [
    // Foundation Vendor
    "bower_components/foundation/js/vendor/fastclick.js",
    "bower_components/foundation/js/vendor/placeholder.js",
    // Foundation Core
    "bower_components/foundation/js/foundation/foundation.js",
    "bower_components/foundation/js/foundation/foundation.abide.js",
    "bower_components/foundation/js/foundation/foundation.accordion.js",
    "bower_components/foundation/js/foundation/foundation.alert.js",
    "bower_components/foundation/js/foundation/foundation.clearing.js",
    "bower_components/foundation/js/foundation/foundation.dropdown.js",
    "bower_components/foundation/js/foundation/foundation.equalizer.js",
    "bower_components/foundation/js/foundation/foundation.interchange.js",
    "bower_components/foundation/js/foundation/foundation.joyride.js",
    "bower_components/foundation/js/foundation/foundation.magellan.js",
    "bower_components/foundation/js/foundation/foundation.offcanvas.js",
    "bower_components/foundation/js/foundation/foundation.orbit.js",
    "bower_components/foundation/js/foundation/foundation.reveal.js",
    "bower_components/foundation/js/foundation/foundation.tab.js",
    "bower_components/foundation/js/foundation/foundation.tooltip.js",
    "bower_components/foundation/js/foundation/foundation.topbar.js",
    'assets/js/plugins/*.js',
    'assets/js/_*.js'
  ];

  grunt.initConfig({
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      all: [
        'Gruntfile.js',
        'assets/js/*.js',
        '!assets/js/scripts.js',
        '!assets/**/*.min.*'
      ]
    },
    sass: {
      options: {
        loadPath: [
          'bower_components/bourbon/dist',
          'bower_components/foundation/scss'
        ]
      },
      dev: {
        options: {
          style: 'expanded'
        },
        files: {
          'assets/css/main.css': 'assets/scss/main.scss'
        }
      },
      build: {
        options: {
          style: 'compressed'
        },
        files: {
          'assets/css/main.min.css': 'assets/scss/main.scss'
        }
      }
    },
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: [jsFileList],
        dest: 'assets/js/scripts.js',
      },
    },
    uglify: {
      dist: {
        files: {
          'assets/js/scripts.min.js': [jsFileList]
        }
      }
    },
    autoprefixer: {
      options: {
        browsers: ['last 2 versions', 'ie 8', 'ie 9', 'android 2.3', 'android 4', 'opera 12']
      },
      dev: {
        options: {
          map: {
            prev: 'assets/css/'
          }
        },
        src: 'assets/css/main.css'
      },
      build: {
        src: 'assets/css/main.min.css'
      }
    },
    modernizr: {
      build: {
        devFile: 'bower_components/modernizr/modernizr.js',
        outputFile: 'assets/js/vendor/modernizr.min.js',
        files: {
          'src': [
            ['assets/js/scripts.min.js'],
            ['assets/css/main.min.css']
          ]
        },
        uglify: true,
        parseFiles: true
      }
    },
    version: {
      default: {
        options: {
          format: true,
          length: 32,
          manifest: 'assets/manifest.json',
          querystring: {
            style: 'roots_css',
            script: 'roots_js'
          }
        },
        files: {
          'lib/scripts.php': 'assets/{css,js}/{main,scripts}.min.{css,js}'
        }
      }
    },
    watch: {
      less: {
        files: [
          'assets/scss/*.scss',
          'assets/scss/**/*.scss'
        ],
        tasks: ['sass:dev', 'autoprefixer:dev']
      },
      js: {
        files: [
          jsFileList,
          '<%= jshint.all %>'
        ],
        tasks: ['jshint', 'concat']
      },
      livereload: {
        // Browser live reloading
        // https://github.com/gruntjs/grunt-contrib-watch#live-reloading
        options: {
          livereload: false
        },
        files: [
          'assets/css/main.css',
          'assets/js/scripts.js',
          'templates/*.php',
          '*.php'
        ]
      }
    }
  });

  // Register tasks
  grunt.registerTask('default', [
    'dev'
  ]);
  grunt.registerTask('dev', [
    'jshint',
    'sass:dev',
    'autoprefixer:dev',
    'concat'
  ]);
  grunt.registerTask('build', [
    'jshint',
    'sass:build',
    'autoprefixer:build',
    'uglify',
    'modernizr',
    'version'
  ]);
};
